==============
rabinpoly-java
==============

A (mostly) line for line translation of rabinpoly, a C library
extracted from LBFS.

See: http://www.cs.cmu.edu/~hakim/software/rabinpoly-README.txt for
a little more information.

Uses 64-bit polynomials.

Default build will generate a jar that can be run to print out rabin
fingerprints generated over a sliding window of bytes for a file.
