package org.cdlib.rabinpoly;

/*
 * Copyright (C) 2009 University of California Regents
 * Copyright (C) 2004 Hyang-Ah Kim (hakim@cs.cmu.edu)
 * Copyright (C) 1999 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

public class Window extends RabinPoly {
    public int size;

    private long fingerprint;
    private int bufpos;
    private long[] U = new long[256];
    private long poly;
    private byte[] buf;
    private long sizeshift = 1L;

    public Window (long poly, int size) {
        super(poly);
        this.size = size;
        this.buf = new byte[size];
        for (int i = 1; i < size; i++)
            sizeshift = this.append8 (sizeshift, (byte) 0);
        for (int i = 0; i < 256; i++)
            U[i] = polymmult (i, sizeshift, poly);
    }
    
    public long slide8 (byte m) {
        if (++this.bufpos >= this.size)
            this.bufpos = 0;
        byte om = buf[bufpos];
        buf[bufpos] = m;
        return fingerprint = this.append8 (fingerprint ^ U[om & 0xff], m);
    }

    public void reset () { 
        this.fingerprint = 0L;
        this.buf = new byte[size];
    }
}
