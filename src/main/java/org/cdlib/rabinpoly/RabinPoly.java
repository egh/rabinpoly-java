package org.cdlib.rabinpoly;

/*
 * Copyright (C) 2009 University of California Regents
 * Copyright (C) 2004 Hyang-Ah Kim (hakim@cs.cmu.edu)
 * Copyright (C) 1999 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class RabinPoly {
    private static long MSB64 = 0x8000000000000000L;

    public static String getBytes (final long l) {
        return String.format("%016x", l);
    }

    /* This seems to be a faster version of fls64 than a translation
       of the original C code */
    /* Find last set (most significant bit) */
    public static int fls64(long v) {
        int r = 1;
        while ((v >>>= 1) != 0) r++;
        return r;
    }
    
    /**
     * Calculate P mod d, where P is a 128-bit polynomial.
     *
     */
    public static long polymod (long nh, long nl, long d) {
        int k = fls64 (d) - 1;
        d <<= 63 - k;

        if (nh != 0) {
            if ((nh & MSB64) != 0) {
                nh ^= d;
            }
            for (int i = 62; i >= 0; i--) {
                if ((nh & ((long) 1) << i) != 0) {
                    nh ^= d >>> (63 - i);
                    nl ^= d << (i + 1);
                }
            }
        }
        for (int i = 63; i >= k; i--) {
            if ((nl & 1L << i) != 0)
                nl ^= d >>> 63 - i;
        }
        return nl;
    }

    public static long polygcd (long x, long y) {
        long x1 = x;
        long y1 = y;
        while (true) {
            if (y1 == 0)
                return x1;
            x1 = polymod (0, x1, y1);
            if (x1 == 0)
                return y1;
            y1 = polymod (0, y1, x1);
        }
    }

    public static class LongPair {
        public long a;
        public long b;
        public LongPair (long a, long b) {
            this.a = a;
            this.b = b;
        }
    }

    /**
     * Multiply 2 64 bit polynomials in GF(2).
     *
     */
    public static LongPair polymult (long x, long y) {
        long ph = 0, pl = 0;
        if ((x & 1) != 0) {
            pl = y;
        }
        for (int i = 1; i < 64; i++) {
            if ((x & (1L << i)) != 0) {
                ph ^= y >>> (64 - i);
                pl ^= y << i;
            }
        }
        return new RabinPoly.LongPair (ph, pl);
    }
    
    public static long polymmult (long x, long y, long d) {
        LongPair lp = polymult (x, y);
        return polymod (lp.a, lp.b, d);
    }

    public static boolean polyirreducible (long f) {
        long u = 2;
        int m = (int) (fls64 (f) - 1) >>> 1;
        for (int i = 0; i < m; i++) {
            u = polymmult (u, u, f);
            if (polygcd (f, u ^ 2) != 1)
                return false;
        }
        return true;
    }

    private int shift;
    private long[] T = new long[256];		// Lookup table for mod
    private final long poly;		// Actual polynomial

    public void calcT () {
        int xshift = fls64 (poly) - 1;
        shift = xshift - 8;
        long T1 = polymod (0, (1L << xshift), poly);
        for (long j = 0; j < 256; j++) {
            T[(int)j] = (polymmult (j, T1, poly) | (j << xshift));
        }
    }

    public RabinPoly (long poly) {
        this.poly = poly;
        assert (polyirreducible(poly));
        calcT ();
    }

    public long append8 (long p, byte m) {
        return ((p << 8) | (m&0xff)) ^ T[(int)(p >>> shift)];
    }

    // value take from LBFS fingerprint.h. For detail on this, 
    // refer to the original rabin fingerprint paper.
    public static final long FINGERPRINT_PT = 0xbfe6b8a5bf378d83L;

    public static long fingerprint(String str) throws UnsupportedEncodingException {
        return fingerprint(str.getBytes("UTF-8"));
    }
    
    /** 
     * Calculate a rabin fingerprint for a byte array.
     *
     * @returns fingerprint long
     */
    public static long fingerprint(byte[] bytes) {
        RabinPoly rb = new RabinPoly(FINGERPRINT_PT);
        long fp = 0L;
        for (int i = 0 ; i < bytes.length ; i++) {
            fp = rb.append8(fp, bytes[i]);
        }
        return fp;
    }

    /** 
     * Calculate a rabin fingerprint for a InputStream.
     *
     * @returns fingerprint long
     */
    public static long fingerprint(InputStream is) throws IOException {
        RabinPoly rb = new RabinPoly(FINGERPRINT_PT);
        long fp = 0L;
        int count;
        byte[] buf = new byte[1024];
            
        while ((count = is.read(buf)) > 0) {
            for (int i = 0; i < count ; i++) {
                fp = rb.append8(fp, buf[i]);
            }
        }
        return fp;
    }


    private static void usage() {
        System.out.println("Usage: xxxx <file-to-read> <sliding-window-size>");
        System.out.println("    Example) xxxx testfile 16");
    }

    public static void main (String[] args) 
        throws IOException {
        if (args.length < 2) {
            usage();
            System.exit(-1);
        } 
        
        File splitFile = new File(args[0]);
        InputStream is = new FileInputStream(splitFile);

        int windowSize = Integer.parseInt(args[1]);
        int count;
        long fp;
        byte[] buf = new byte[1024];		// buffer to read data in
        
        Window myRabin = new Window(FINGERPRINT_PT, windowSize);
        myRabin.reset();
        while ((count = is.read(buf)) > 0) {
            for (int i = 0; i < count ; i++) {
                fp = myRabin.slide8(buf[i]);
                System.out.print(String.format("%s\n", getBytes(fp)));
            }
        }
    }
}
